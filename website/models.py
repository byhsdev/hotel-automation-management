#
# This file is part of "Hotel Automation Management".
#
#    Hotel Automation Management is free software: you can redistribute it and/or modify
#    it under the terms of the GNU General Public License as published by
#    the Free Software Foundation, either version 3 of the License, or
#    (at your option) any later version.
#
#    Hotel Automation Management is distributed in the hope that it will be useful,
#    but WITHOUT ANY WARRANTY; without even the implied warranty of
#    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
#    GNU General Public License for more details.
#
#    You should have received a copy of the GNU General Public License
#    along with Hotel Automation Management.  If not, see <http://www.gnu.org/licenses/>.
#
#    Author: Antonio Sanchez Pineda

__author__ = "Antonio Sanchez Pineda"
__copyright__ = "Copyright 2013, Hotel Automation Management"
__credits__ = ["Antonio Sanchez Pineda"]
__license__ = "GPL"
__version__ = "0.2.1"
__maintainer__ = "Antonio Sanchez Pineda"
__email__ = "iam@sanchezantonio.com"
__status__ = "Development"

from django.db import models

class Humidity2(models.Model):
    value = models.IntegerField()
    date = models.DateTimeField(auto_now_add=True)

    def __str__(self):
        return str(self.value)

class Relax2(models.Model):
    value = models.CharField(max_length=10)
    date = models.DateTimeField(auto_now_add=True)

    def __str__(self):
        return self.value
