#
# This file is part of "Hotel Automation Management".
#
#    Hotel Automation Management is free software: you can redistribute it and/or modify
#    it under the terms of the GNU General Public License as published by
#    the Free Software Foundation, either version 3 of the License, or
#    (at your option) any later version.
#
#    Hotel Automation Management is distributed in the hope that it will be useful,
#    but WITHOUT ANY WARRANTY; without even the implied warranty of
#    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
#    GNU General Public License for more details.
#
#    You should have received a copy of the GNU General Public License
#    along with Hotel Automation Management.  If not, see <http://www.gnu.org/licenses/>.
#
#    Author: Antonio Sanchez Pineda

__author__ = "Antonio Sanchez Pineda"
__copyright__ = "Copyright 2013, Hotel Automation Management"
__credits__ = ["Antonio Sanchez Pineda"]
__license__ = "GPL"
__version__ = "0.2.1"
__maintainer__ = "Antonio Sanchez Pineda"
__email__ = "iam@sanchezantonio.com"
__status__ = "Development"

from django.shortcuts import render_to_response, HttpResponseRedirect
from django.template import Context, RequestContext
from django.http import HttpResponse, HttpResponseForbidden
from myproject.website.models import *
from myproject.website import minidom
import requests
import time

def __to_warm():
    status_code = []
    for n in range(0, 10):
        url = ("http://194.73.233.52:8083/ZWaveAPI/Run/devices[8].instances[2].commandClasses[0x26].Set(%s)" % str(n*10))
        r = requests.get(url, timeout=2)
        status_code.append(r.status_code)

        url = "http://194.73.233.52:8083/ZWaveAPI/Run/devices[8].instances[3].commandClasses[0x26].Set(0)"
        r = requests.get(url, timeout=2)
        status_code.append(r.status_code)

        url = ("http://194.73.233.52:8083/ZWaveAPI/Run/devices[8].instances[4].commandClasses[0x26].Set(%s)" % str(100-(n*10)))
        r = requests.get(url, timeout=2)
        status_code.append(r.status_code)
        time.sleep(.5)
    return status_code

def __to_cold():
    status_code = []
    for n in range(0, 10):
        url = ("http://194.73.233.52:8083/ZWaveAPI/Run/devices[8].instances[2].commandClasses[0x26].Set(%s)" % str(100-(n*10)))
        r = requests.get(url, timeout=2)
        status_code.append(r.status_code)

        url = "http://194.73.233.52:8083/ZWaveAPI/Run/devices[8].instances[3].commandClasses[0x26].Set(0)"
        r = requests.get(url, timeout=2)
        status_code.append(r.status_code)

        url = ("http://194.73.233.52:8083/ZWaveAPI/Run/devices[8].instances[4].commandClasses[0x26].Set(%s)" % str(n*10))
        r = requests.get(url, timeout=2)
        status_code.append(r.status_code)
        time.sleep(.5)
    return status_code

def index(request):
    context = {}
    humidity = Humidity2.objects.all().order_by('-date')[0]
    relax = Relax2.objects.all().order_by('-date')[0]
    context['humidity'] = humidity.value
    context['relax'] = relax.value
    return render_to_response("index.html", context, context_instance=RequestContext(request))

def about(request):
    context = {}
    return render_to_response("about.html", context, context_instance=RequestContext(request))

def manual_towel(request):
    context = {}
    status_code = __to_warm()

    context['status_code'] = status_code

    return render_to_response("relax.html", context, context_instance=RequestContext(request))

def relax(request, status):
    context = {}
    status_code = []

    rel = Relax2()
    rel.value = status
    rel.save()

    if status == 'on':
        context['status'] = 'on'
        url = "http://194.73.233.52:8083/ZWaveAPI/Run/devices[8].instances[2].commandClasses[0x26].Set(99)"
        r = requests.get(url, timeout=2)
        status_code.append(r.status_code)

        url = "http://194.73.233.52:8083/ZWaveAPI/Run/devices[8].instances[3].commandClasses[0x26].Set(0)"
        r = requests.get(url, timeout=2)
        status_code.append(r.status_code)

        url = "http://194.73.233.52:8083/ZWaveAPI/Run/devices[8].instances[4].commandClasses[0x26].Set(0)"
        r = requests.get(url, timeout=2)
        status_code.append(r.status_code)
    else :
        context['status'] = 'off'
        url = "http://194.73.233.52:8083/ZWaveAPI/Run/devices[8].instances[2].commandClasses[0x26].Set(10)"
        r = requests.get(url, timeout=2)
        status_code.append(r.status_code)

        url = "http://194.73.233.52:8083/ZWaveAPI/Run/devices[8].instances[3].commandClasses[0x26].Set(10)"
        r = requests.get(url, timeout=2)
        status_code.append(r.status_code)

        url = "http://194.73.233.52:8083/ZWaveAPI/Run/devices[8].instances[4].commandClasses[0x26].Set(10)"
        r = requests.get(url, timeout=2)
        status_code.append(r.status_code)


    context['status_code'] = status_code

    return render_to_response("relax.html", context, context_instance=RequestContext(request))

def sensor_4in1_bedroom(request):
    context = {}
    return render_to_response("relax.html", context, context_instance=RequestContext(request))

def sensor_4in1_bathroom(request):
    context = {}
    return render_to_response("relax.html", context, context_instance=RequestContext(request))

#Comes from callback url, sended by IDAS.
def dimmer_dehumidifier(request, status):
    context = {}
    status_code = []

    if status == 'on':
        url = "http://194.73.233.52:8083/ZWaveAPI/Run/devices[4].instances[0].commandClasses[0x26].Set(255)"
    else:
        url = "http://194.73.233.52:8083/ZWaveAPI/Run/devices[4].instances[0].commandClasses[0x26].Set(0)"
    r = requests.get(url, timeout=2)
    status_code.append(r.status_code)

    context['status_code'] = status_code
    return render_to_response("relax.html", context, context_instance=RequestContext(request))
    

#Comes from callback url, sended by IDAS.
def door_trigger(request, status):
    context = {}
    status_code = []

    if status == 'triggered':
        status_code = __to_warm() #warm the towel.
    else :
        status_code = __to_cold() #stops the towel calefaction.

    context['status_code'] = status_code

    return render_to_response("relax.html", context, context_instance=RequestContext(request))


def movement_trigger(request, status):
    """ Turn on the light when somebody enter at the bathroom. """
    context = {}
    status_code = []

    if status == 'triggered':
        url = "http://194.73.233.52:8083/ZWaveAPI/Run/devices[4].instances[0].commandClasses[0x26].Set(255)"
        r = requests.get(url, timeout=2)
        status_code.append(r.status_code)
    else :
        url = "http://194.73.233.52:8083/ZWaveAPI/Run/devices[4].instances[0].commandClasses[0x26].Set(0)"
        r = requests.get(url, timeout=2)
        status_code.append(r.status_code)

    context['status_code'] = status_code
    return render_to_response("relax.html", context, context_instance=RequestContext(request))

def accumulate(request):
    #TODO: movement in bathroom turn on lights.
    #TODO: humidity on bedroom turn on humidifier.
    #TODO: open the door in bathroom turn on the towel calefactor.
    if request.POST:
        
        xmlfile = minidom.parse(request.POST)
        id_device = xmlfile.getElementByTagName('id')[0].childNodes[0].data
        
        if id_device == "TALENTUMMLG.4IN1:65:FA:11:0003":
            humidity = xmlfile.getElementByTagName('contextAttribute')[-1].childNodes[2].data
            if humidity >= 70:
                url = "http://194.73.233.52:8083/ZWaveAPI/Run/devices[4].instances[0].commandClasses[0x26].Set(255)"
                r = requests.get(url, timeout=2)
            hum = Humidity2()
            hum.value = humidity
            hum.value()
        return HttpResponse()
        
    else:
        return HttpResponseForbidden()
    

