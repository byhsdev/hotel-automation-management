Automation Hotel Management is a project developed in just 72 hours during the Fi-Ware hackathon held in the Campus Party Europe 2013.

Winner of "Best IoT-Application of Fi-Ware".

The project consists of the intelligent management of a hotel room through the domotization of them, using z-wave devices and a Raspberry Pi.

Furthermore, this whole project would not be possible without the Fi -Ware platform, which provides a lot of services essential to the proper functioning of the project.

The code has not changed its functionality or refactored after the hackathon, and we understand that although the quality of code is not excellent, it's interesting to see at what level it has managed to get on the circumstances in which it has developed.

Important read the license terms, which is distributed under GPL.

The project has been developed by Antonio Sánchez Pineda, as to all the server side. And Daniel López Pedrosa has designed the templates (html and css).


=== SPANISH ===

Hotel Automation Management es un proyecto desarrollado en tan sólo 72 horas durante la Hackaton de Fi-Ware celebrada en la Campus Party Europe 2013.

Ganador del premio "Best IoT-Application of Fi-Ware".

El proyecto consiste en la gestión inteligente de las habitaciones de un hotel gracias a la domotización de las mismas, utilizando para ello dispositivos z-wave y una Raspberry Pi.

Además, todo este proyecto no sería posible sin contar con la plataforma Fi-Ware, la cual proveé una gran cantidad de servicios indispensables para el correcto funcionamiento del proyecto.

El código no ha modificado su funcionalidad ni refactorizado una vez finalizada la Hackaton, ya que entendemos que aunque la calidad del código no sea excelente, es interesante ver a qué nivel del mismo se ha conseguido llegar en las circumstancias en las que se ha desarrollado.

Importante leer los términos de licencia, el cual se distribuye bajo GPL.

El proyecto ha sido desarrollado por Antonio Sánchez Pineda, en cuanto a toda la parte del servidor. Y Daniel López Pedrosa ha diseñado los templates (html y css).
