#
# This file is part of "Hotel Automation Management".
#
#    Hotel Automation Management is free software: you can redistribute it and/or modify
#    it under the terms of the GNU General Public License as published by
#    the Free Software Foundation, either version 3 of the License, or
#    (at your option) any later version.
#
#    Hotel Automation Management is distributed in the hope that it will be useful,
#    but WITHOUT ANY WARRANTY; without even the implied warranty of
#    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
#    GNU General Public License for more details.
#
#    You should have received a copy of the GNU General Public License
#    along with Hotel Automation Management.  If not, see <http://www.gnu.org/licenses/>.
#
#    Author: Antonio Sanchez Pineda

__author__ = "Antonio Sanchez Pineda"
__copyright__ = "Copyright 2013, Hotel Automation Management"
__credits__ = ["Antonio Sanchez Pineda"]
__license__ = "GPL"
__version__ = "0.2.1"
__maintainer__ = "Antonio Sanchez Pineda"
__email__ = "iam@sanchezantonio.com"
__status__ = "Development"


from django.conf.urls import patterns, include, url
from myproject.website.views import *
from django.contrib import admin
from os import path
admin.autodiscover()

# Obtenemos la raiz del proyecto (en el cual se encuentra settins.py) y sustituimos los slashes por si estuviera en Windows
BASEDIR = path.dirname(path.abspath(__file__))

urlpatterns = patterns('',
	#Para local solo
	(r'^media/(.*)$', 'django.views.static.serve', {'document_root': path.join(BASEDIR, 'media')}),

    url(r'^(?i)relax/(?P<status>\w+)', relax),
    url(r'^(?i)4in1/bedroom', sensor_4in1_bedroom),
    url(r'^(?i)4in1/bathroom', sensor_4in1_bathroom),
    url(r'^(?i)dimmer/(?P<status>\w+)', dimmer_dehumidifier),
    url(r'^(?i)door/(?P<status>\w+)', door_trigger), #callback url, from IDAS. Must be subscribe.
    url(r'^(?i)movement/(?P<status>\w+)', movement_trigger),
    url(r'^(?i)accumulate', accumulate),
    url(r'^(?i)towel', manual_towel),
    url(r'^(?i)about', about),

    # Uncomment the admin/doc line below to enable admin documentation:
    url(r'^admin/doc/', include('django.contrib.admindocs.urls')),

    # Uncomment the next line to enable the admin:
    url(r'^admin/', include(admin.site.urls)),

    url(r'^$', index),
	url(r'^', index),
)
